# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and Pieter
# Abbeel in Spring 2013.
# For more info, see http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html

from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        foodList = newFood.asList()
        numCurrFood = len(currentGameState.getFood().asList())
        numNewFood = len(foodList)

        # find the closest food, or set value high if no food/on food
        food = [util.manhattanDistance(newPos, food) for food in foodList]
        if len(food):
            foodVal = min(food)
        else:
            foodVal = 100

        # find the closest ghost
        ghostPos = []
        for ghost in newGhostStates:
            ghostPos.append(ghost.getPosition())
        ghosts = [util.manhattanDistance(newPos, ghost) for ghost in ghostPos]
        ghostVal = min(ghosts)

        # prioritize ghost avoidance, then acquiring closest food
        returnVal = 0
        if ghostVal < 3: returnVal = -100
        elif numCurrFood != numNewFood: returnVal = 100
        else: returnVal = foodVal**-1
##        print"Ghost value: %f" %ghostVal
##        print"Food value: %f" %foodVal
##        print "Return value: %f\n" %returnVal
        return returnVal



def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        agents = gameState.getNumAgents()
        plies = self.depth
        depth = plies * agents

        # find legal actions and their successors
        LegalActions = gameState.getLegalActions(0)
        if Directions.STOP in LegalActions: LegalActions.remove(Directions.STOP)
        successors = []
        for action in LegalActions:
            successors.append(gameState.generateSuccessor(0,action))

        # get the value for each successor
        values = []
        for state in successors:
            values.append(self.valueFN(1, state, depth-1))
        maxValue = max(values)

        # return a random best-action
        maxIndices = []
        for i in range(0,len(values)):
            if values[i] == maxValue: maxIndices.append(i)
        return LegalActions[random.choice(maxIndices)]


    def valueFN(self, agentIndex, gameState, depth):
        numAgents = gameState.getNumAgents()
        # if endstate or depth reached
        if (gameState.isWin() or gameState.isLose() or depth==0):
              return self.evaluationFunction(gameState)
        LegalActions = gameState.getLegalActions(agentIndex)
        if Directions.STOP in LegalActions: LegalActions.remove(Directions.STOP)
        successors = [gameState.generateSuccessor(agentIndex, action) for action in LegalActions]

        # max-value
        if agentIndex == 0:
            return max([self.valueFN((agentIndex+1)%numAgents, state, depth-1) for state in successors])
        # min-value
        else :
            return min([self.valueFN((agentIndex+1)%numAgents, state, depth-1) for state in successors])




class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        alpha, beta = float("-inf"), float("inf")
        agents = gameState.getNumAgents()
        plies = self.depth
        depth = plies * agents

        # if endstate or depth reached
        if (gameState.isWin() or gameState.isLose() or depth==0):
              return self.evaluationFunction(gameState)

        # find legal actions and their successors
        LegalActions = gameState.getLegalActions(0)
        if Directions.STOP in LegalActions: LegalActions.remove(Directions.STOP)
        bestAction = None
        bestVal = float("-inf")


        # among legal actions, find the best
        for action in LegalActions:
            successor = gameState.generateSuccessor(0, action)
            value = self.valueFN((1)%agents, successor, depth-1, alpha, beta)
            if value > bestVal: bestVal, bestAction = value, action
            alpha = max(alpha, value)
        return bestAction


    def valueFN(self, agentIndex, gameState, depth, a = float("-inf"), b = float("inf")):
        numAgents = gameState.getNumAgents()
        # if endstate or depth reached
        if (gameState.isWin() or gameState.isLose() or depth==0):
              return self.evaluationFunction(gameState)
        LegalActions = gameState.getLegalActions(agentIndex)
        if Directions.STOP in LegalActions: LegalActions.remove(Directions.STOP)
        # max-value
        if agentIndex == 0:
            value = float("-inf")
            for action in LegalActions:
                successor = gameState.generateSuccessor(agentIndex, action)
                value = max(value, self.valueFN((agentIndex+1)%numAgents, successor, depth-1, a, b))
                if value > b: return value
                a = max(a, value)
            return value
        # min-value
        else :
            value = float("inf")
            for action in LegalActions:
                successor = gameState.generateSuccessor(agentIndex, action)
                value = min(value, self.valueFN((agentIndex+1)%numAgents, successor, depth-1, a, b))
                if value < a: return value
                b = min(b, value)
            return value


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        agents = gameState.getNumAgents()
        plies = self.depth
        depth = plies * agents

        # find legal actions and their successors
        LegalActions = gameState.getLegalActions(0)
        if Directions.STOP in LegalActions: LegalActions.remove(Directions.STOP)
        successors = []
        for action in LegalActions:
            successors.append(gameState.generateSuccessor(0,action))

        # get the value for each successor
        values = []
        for state in successors:
            values.append(self.valueFN(1, state, depth-1))
        maxValue = max(values)

        # return a random best-action
        maxIndices = []
        for i in range(0,len(values)):
            if values[i] == maxValue: maxIndices.append(i)
        return LegalActions[random.choice(maxIndices)]


    def valueFN(self, agentIndex, gameState, depth):
        numAgents = gameState.getNumAgents()
        # if endstate or depth reached
        if (gameState.isWin() or gameState.isLose() or depth==0):
              return self.evaluationFunction(gameState)
        LegalActions = gameState.getLegalActions(agentIndex)
        if Directions.STOP in LegalActions: LegalActions.remove(Directions.STOP)
        successors = [gameState.generateSuccessor(agentIndex, action) for action in LegalActions]

        # max-value
        if agentIndex == 0:
            return max([self.valueFN((agentIndex+1)%numAgents, state, depth-1) for state in successors])
        # exp-value
        else :
            values = [self.valueFN((agentIndex+1)%numAgents, state, depth-1) for state in successors]
            return float(sum(values))/len(values)



def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION:
      - Avoids ghosts within radius threshold
      - Avoids corners and walls when ghosts are within threshold
      - If ghost is scared, ignores ghost position while timer is high
      - Prioritizes closest food otherwise

    ===== VERY BUGGY =====

    """

    pos = currentGameState.getPacmanPosition()
    walls = currentGameState.getWalls()
    food = currentGameState.getFood().asList()
    numGhosts = currentGameState.getNumAgents() - 1
    scaredTimes = [ghostState.scaredTimer for ghostState in currentGameState.getGhostStates()]
    foodCount = currentGameState.getNumFood()

    def wallNum():
        """
        Returns the numbers of walls surrounding
        Pacman in the current state, plus 1 (for division).
        """
        count = 1
        moves = [(pos[0], pos[1]+1), (pos[0]+1, pos[1]), (pos[0], pos[1]-1), (pos[0]-1, pos[1])]
        for p in moves:
            try:
                if walls[p[0]][p[1]]:
                    count += 1
            except:
                pass
        return count

    def closestGhostIndex():
        """
        Returns the index of the closest non-scared ghost,
        or -1 if all are scared. If scared time is low,
        returns the index of the closest ghost.
        """
        minDist = float("inf")
        index = -1
        for i in range(1, numGhosts):
            if min(scaredTimes) > 1:
                if currentGameState.getGhostState(i).scaredTimer: continue
            ghost = currentGameState.getGhostPosition(i)
            dist = eDist(ghost)
            if dist < minDist:
                minDist = dist
                index = i
        return index

    def closestGhostDist():
        """
        Returns the distance of the nearest
        non-scared ghost if the timer is high,
        or of any ghost if the timer is low
        """
        ghost = currentGameState.getGhostPosition(closestGhostIndex())
        return eDist(ghost)

    def simpleGhostDist():
        minDist = float("inf")
        for ghost in currentGameState.getGhostPositions():
            dist = eDist(ghost)
            minDist = min(minDist, dist)
        return minDist

    def closestFood():
        """
        Returns tuple of:
            the number of close foods
            the distance to the closest food(s)
        """
        minDist = float("inf")
        count = 0
        for morsel in food:
            dist = eDist(morsel)
            if dist == minDist:
                count += 1
            elif dist < minDist:
                count = 1
                minDist = dist
        return (count, minDist)

    def eDist(itemPos):
        """
        Returns the euclidean distance between Pacman and an item
        """
        return ((pos[0] - itemPos[0])**2 + (pos[1] - itemPos[1])**2)**0.5


    returnVal = 0
    if closestGhostDist() <= 3: return -1000
    elif closestFood()[1] <= 1: return 1000
    else: return closestFood()[0]*(closestFood()[1]**-1)

# Abbreviation
better = betterEvaluationFunction

class ContestAgent(MultiAgentSearchAgent):
    """
      Your agent for the mini-contest
    """

    def getAction(self, gameState):
        """
          Returns an action.  You can use any method you want and search to any depth you want.
          Just remember that the mini-contest is timed, so you have to trade off speed and computation.

          Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
          just make a beeline straight towards Pacman (or away from him if they're scared!)
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

